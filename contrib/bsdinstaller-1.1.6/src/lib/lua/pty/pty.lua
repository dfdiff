-- $Id: pty.lua,v 1.1 2005/04/03 19:52:01 cpressey Exp $
-- Lua wrapper functions for Lua 5.0.x Pty (pseudo-terminal) binding.

module("pty")

Pty = require("lpty")

return Pty
