-- $Id: main.lua,v 1.6 2005/02/02 22:26:20 cpressey Exp $

require "dfui"

Menu.auto{
    name = "Submenu",
    short_desc =
	"This is a submenu automatically " ..
	"created from a subdirectory."
}
