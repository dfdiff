/* $DragonFly: src/gnu/usr.bin/gdb/libbfd/bfdver.h,v 1.2 2008/01/14 21:36:38 corecode Exp $ */
#define BFD_VERSION_DATE 20070907
#define BFD_VERSION 218500000
#define BFD_VERSION_STRING  "(GNU Binutils)" "2.18.50.20070907"
#define REPORT_BUGS_TO "<http://bugs.dragonflybsd.org/>"
