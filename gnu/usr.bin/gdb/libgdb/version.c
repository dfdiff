/* $DragonFly: src/gnu/usr.bin/gdb/libgdb/version.c,v 1.2 2008/01/14 21:36:38 corecode Exp $ */
#include "version.h"
const char version[] = "6.7.1";
const char host_name[] = MACHINE_ARCH"-dragonfly";
const char target_name[] = MACHINE_ARCH"-dragonfly";
