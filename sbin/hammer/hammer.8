.\" Copyright (c) 2007 The DragonFly Project.  All rights reserved.
.\" 
.\" This code is derived from software contributed to The DragonFly Project
.\" by Matthew Dillon <dillon@backplane.com>
.\" 
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in
.\"    the documentation and/or other materials provided with the
.\"    distribution.
.\" 3. Neither the name of The DragonFly Project nor the names of its
.\"    contributors may be used to endorse or promote products derived
.\"    from this software without specific, prior written permission.
.\" 
.\" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
.\" ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
.\" LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
.\" FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
.\" COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING,
.\" BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
.\" LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
.\" AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
.\" OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
.\" OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\" 
.\" $DragonFly: src/sbin/hammer/hammer.8,v 1.12 2008/05/04 19:18:17 dillon Exp $
.Dd December 31, 2007
.Dt HAMMER 8
.Os
.Sh NAME
.Nm hammer
.Nd HAMMER file system utility
.Sh SYNOPSIS
.Nm
.Op Fl hrx
.Op Fl f Ar blkdev[:blkdev]*
.Op Fl s Ar linkpath
.Op Fl t Ar timeout
.Ar command
.Ar ...
.Sh DESCRIPTION
The
.Nm
utility provides miscellaneous functions related to managing a HAMMER
filesystem.
.Pp
The options are as follows:
.Bl -tag -width indent
.It Fl h
Get help
.It Fl r
Specify recursion for those commands which support it.
.It Fl f Ar blkdev[:blkdev]*
Specify the volumes making up a HAMMER filesystem.
.It Fl s Ar linkpath
When pruning a filesystem you can instruct HAMMER to create softlinks
to available snapshots.
.It Fl t Ar timeout
When pruning and reblocking you can tell the utility to stop after a
certain period of time.
This option is typically used to limit the time spent reblocking.
.It Fl v
Increase verboseness.  May be specified multiple times.
.It Fl x
Do not call sync() when running commands which sync() by default.
Timestamp commands such as 'hammer now' sync() by default.  This also
disables any sleeps the timestamp commands would otherwise perform.
.El
.Pp
The commands are as follows:
.Bl -tag -width indent
.It Ar now
Generate a timestamp suitable for use in the @@ filename extension,
representing right now.
Unless
.Fl x
is specified, this command will automatically sync() and
wait for the seconds hand to turn over (sleep for up to one second) prior
to generating a seconds-denominated timestamp.
.It Ar now64
Generate a full 64 bit timestamp.
Unless
.Fl x
is specified, this command will automatically sync(), but not sleep,
prior to generating the timestamp.
.It Ar stamp
Generate a timestamp suitable for use in the @@ filename extension.
This command does not sync() or sleep and care should be taken if
generating timestamps for data which may not yet be synced to disk.
A time specification of
.Pf yyyymmdd Oo :hhmmss Oc Ns Op .fractional
specifies an exact as-of timestamp in local (not UTC) time.
Set the TZ environment variable prior to running
.Nm
if you wish to specify the time by some other means.
.It Ar stamp64
Same as the
.Ar stamp
command but generates a 64 bit timestamp.
.It Ar history Ar path
Show the modification history for a HAMMER file's inode and data.
.It Ar show Op vol_no[:clu_no]
Dump the B-Tree starting at the specified volume and cluster, or
at the root volume if not specified.
The B-Tree is dumped recursively if the
.Fl r
option is specified.
.It Ar blockmap
Dump the btree, record, large-data, and small-data blockmaps, showing
physical block assignments and free space percentages.
.It Ar namekey Ar filename
Generate a HAMMER 64 bit directory hash for the specified file name.
The low 32 bits are used as an iterator for hash collisions and will be
output as 0.
.It Ar namekey32 Ar filename
Generate the top 32 bits of a HAMMER 64 bit directory hash for the specified
file name.
.It Ar prune Ar filesystem Ar from Ar #{smhdMy} Ar to Ar #{smhdMy} Ar every Ar #{smhdMy}
.It Ar prune Ar filesystem Ar everything
.It Ar prune Ar filesystem Op Ar using Ar filename
Prune the filesystem, removing deleted records to free up physical disk
space.  Specify a time range between the nearest modulo 0 boundary
and prune the tree to the specified granularity within that range.
.Pp
The filesystem specification should be the root of any mounted HAMMER
filesystem.  This command uses a filesystem ioctl to issue the pruning
operation.  If you specify just the filesystem with no other parameters
all prune directives matching that filesystem in the /etc/hammer.conf file
will be used.  If you specify a
.Ar using
file then those directives contained in the file matching
.Ar filesystem
will be used.  Multiple directives may be specified when extracting from
a file.  The directives must be in the same format: "prune ....", in
ascending time order (per filesystem).  Matching prune elements must not
have overlapping time specifications.
.Pp
Both the "from" and the "to" value must be an integral multiple
of the "every" value, and the "to" value must be an integral multiple
of the "from" value.  When you have multiple pruning rules you must
take care to ensure that the range being pruned does not overlap ranges
pruned later on, when the retained data is older.  If they do the retained
data can wind up being destroyed.  For example, if you prune your data
on a 30 minute granularity for the last 24 hours any later pruning must
use a granularity that is a multiple of 30 minutes.  If you prune your
data on a 30 minute boundary, then a 1 day boundary in a later pruning (on
older data), then a pruning beyond that would have to be a multiple of
1 day.  And so forth.
.Pp
The "prune <filesystem> everything" command will remove all historical records
from the filesystem.  The long keyword is designed to prevent accidental use.
This option is not recommended.
.Pp
Example: "hammer prune /mnt from 1h to 1d every 30m"
.El
.Sh EXAMPLES
.Sh DIAGNOSTICS
Exit status is 0 on success and 1 on error.
.Sh SEE ALSO
.Xr newfs_hammer 8
.Sh HISTORY
The
.Nm
utility first appeared in
.Dx 1.11 .
.Sh AUTHORS
.An Matthew Dillon Aq dillon@backplane.com
