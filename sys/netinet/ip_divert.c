/*
 * Copyright (c) 1982, 1986, 1988, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD: src/sys/netinet/ip_divert.c,v 1.42.2.6 2003/01/23 21:06:45 sam Exp $
 * $DragonFly: src/sys/netinet/ip_divert.c,v 1.36 2008/03/07 11:34:20 sephe Exp $
 */

#include "opt_inet.h"
#include "opt_ipfw.h"
#include "opt_ipdivert.h"
#include "opt_ipsec.h"

#ifndef INET
#error "IPDIVERT requires INET."
#endif

#include <sys/param.h>
#include <sys/kernel.h>
#include <sys/malloc.h>
#include <sys/mbuf.h>
#include <sys/socket.h>
#include <sys/protosw.h>
#include <sys/socketvar.h>
#include <sys/sysctl.h>
#include <sys/systm.h>
#include <sys/proc.h>
#include <sys/thread2.h>
#ifdef SMP
#include <sys/msgport.h>
#endif

#include <vm/vm_zone.h>

#include <net/if.h>
#include <net/route.h>
#ifdef SMP
#include <net/netmsg2.h>
#endif

#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/in_pcb.h>
#include <netinet/in_var.h>
#include <netinet/ip_var.h>

/*
 * Divert sockets
 */

/*
 * Allocate enough space to hold a full IP packet
 */
#define	DIVSNDQ		(65536 + 100)
#define	DIVRCVQ		(65536 + 100)

#define DIV_IS_OUTPUT(sin)	((sin) == NULL || (sin)->sin_addr.s_addr == 0)

#define DIV_OUTPUT	0x10000
#define DIV_INPUT	0x20000

/*
 * Divert sockets work in conjunction with ipfw, see the divert(4)
 * manpage for features.
 * Internally, packets selected by ipfw in ip_input() or ip_output(),
 * and never diverted before, are passed to the input queue of the
 * divert socket with a given 'divert_port' number (as specified in
 * the matching ipfw rule), and they are tagged with a 16 bit cookie
 * (representing the rule number of the matching ipfw rule), which
 * is passed to process reading from the socket.
 *
 * Packets written to the divert socket are again tagged with a cookie
 * (usually the same as above) and a destination address.
 * If the destination address is INADDR_ANY then the packet is
 * treated as outgoing and sent to ip_output(), otherwise it is
 * treated as incoming and sent to ip_input().
 * In both cases, the packet is tagged with the cookie.
 *
 * On reinjection, processing in ip_input() and ip_output()
 * will be exactly the same as for the original packet, except that
 * ipfw processing will start at the rule number after the one
 * written in the cookie (so, tagging a packet with a cookie of 0
 * will cause it to be effectively considered as a standard packet).
 */

/* Internal variables */
static struct inpcbinfo divcbinfo;

static u_long	div_sendspace = DIVSNDQ;	/* XXX sysctl ? */
static u_long	div_recvspace = DIVRCVQ;	/* XXX sysctl ? */

/*
 * Initialize divert connection block queue.
 */
void
div_init(void)
{
	in_pcbinfo_init(&divcbinfo);
	/*
	 * XXX We don't use the hash list for divert IP, but it's easier
	 * to allocate a one entry hash list than it is to check all
	 * over the place for hashbase == NULL.
	 */
	divcbinfo.hashbase = hashinit(1, M_PCB, &divcbinfo.hashmask);
	divcbinfo.porthashbase = hashinit(1, M_PCB, &divcbinfo.porthashmask);
	divcbinfo.wildcardhashbase = hashinit(1, M_PCB,
					      &divcbinfo.wildcardhashmask);
	divcbinfo.ipi_zone = zinit("divcb", sizeof(struct inpcb),
				   maxsockets, ZONE_INTERRUPT, 0);
}

/*
 * IPPROTO_DIVERT is not a real IP protocol; don't allow any packets
 * with that protocol number to enter the system from the outside.
 */
void
div_input(struct mbuf *m, ...)
{
	ipstat.ips_noproto++;
	m_freem(m);
}

struct lwkt_port *
div_soport(struct socket *so, struct sockaddr *nam,
	   struct mbuf **mptr, int req)
{
	struct sockaddr_in *sin;
	struct mbuf *m;
	int dir;

	/* Except for send(), everything happens on CPU0 */
	if (req != PRU_SEND)
		return cpu0_soport(so, nam, mptr, req);

	sin = (struct sockaddr_in *)nam;
	m = *mptr;
	M_ASSERTPKTHDR(m);

	m->m_pkthdr.rcvif = NULL;
	dir = DIV_IS_OUTPUT(sin) ? IP_MPORT_OUT : IP_MPORT_IN;

	if (sin != NULL) {
		int i;

		/*
		 * Try locating the interface, if we originally had one.
		 * This is done even for outgoing packets, since for a
		 * forwarded packet, there must be an interface attached.
		 *
		 * Find receive interface with the given name, stuffed
		 * (if it exists) in the sin_zero[] field.
		 * The name is user supplied data so don't trust its size
		 * or that it is zero terminated.
		 */
		for (i = 0; sin->sin_zero[i] && i < sizeof(sin->sin_zero); i++)
			;
		if (i > 0 && i < sizeof(sin->sin_zero))
			m->m_pkthdr.rcvif = ifunit(sin->sin_zero);
	}

	if (dir == IP_MPORT_IN && m->m_pkthdr.rcvif == NULL) {
		/*
		 * No luck with the name, check by IP address.
		 * Clear the port and the ifname to make sure
		 * there are no distractions for ifa_ifwithaddr.
		 *
		 * Be careful not to trash sin->sin_port; it will
		 * be used later in div_output().
		 */
		struct ifaddr *ifa;
		u_short sin_port;

		bzero(sin->sin_zero, sizeof(sin->sin_zero));
		sin_port = sin->sin_port; /* save */
		sin->sin_port = 0;
		ifa = ifa_ifwithaddr((struct sockaddr *)sin);
		if (ifa == NULL) {
			m_freem(m);
			*mptr = NULL;
			return NULL;
		}
		sin->sin_port = sin_port; /* restore */
		m->m_pkthdr.rcvif = ifa->ifa_ifp;
	}

	return ip_mport(mptr, dir);
}

/*
 * Divert a packet by passing it up to the divert socket at port 'port'.
 *
 * Setup generic address and protocol structures for div_input routine,
 * then pass them along with mbuf chain.
 */
static void
div_packet(struct mbuf *m, int incoming, int port)
{
	struct sockaddr_in divsrc = { sizeof divsrc, AF_INET };
	struct inpcb *inp;
	struct socket *sa;
	struct m_tag *mtag;
	u_int16_t nport;

	/* Locate the divert tag */
	mtag = m_tag_find(m, PACKET_TAG_IPFW_DIVERT, NULL);
	divsrc.sin_port = *(u_int16_t *)m_tag_data(mtag);

	/*
	 * Record receive interface address, if any.
	 * But only for incoming packets.
	 */
	divsrc.sin_addr.s_addr = 0;
	if (incoming) {
		struct ifaddr_container *ifac;

		/* Find IP address for receive interface */
		TAILQ_FOREACH(ifac, &m->m_pkthdr.rcvif->if_addrheads[mycpuid],
			      ifa_link) {
			struct ifaddr *ifa = ifac->ifa;

			if (ifa->ifa_addr == NULL)
				continue;
			if (ifa->ifa_addr->sa_family != AF_INET)
				continue;
			divsrc.sin_addr =
			    ((struct sockaddr_in *) ifa->ifa_addr)->sin_addr;
			break;
		}
	}
	/*
	 * Record the incoming interface name whenever we have one.
	 */
	if (m->m_pkthdr.rcvif) {
		/*
		 * Hide the actual interface name in there in the
		 * sin_zero array. XXX This needs to be moved to a
		 * different sockaddr type for divert, e.g.
		 * sockaddr_div with multiple fields like
		 * sockaddr_dl. Presently we have only 7 bytes
		 * but that will do for now as most interfaces
		 * are 4 or less + 2 or less bytes for unit.
		 * There is probably a faster way of doing this,
		 * possibly taking it from the sockaddr_dl on the iface.
		 * This solves the problem of a P2P link and a LAN interface
		 * having the same address, which can result in the wrong
		 * interface being assigned to the packet when fed back
		 * into the divert socket. Theoretically if the daemon saves
		 * and re-uses the sockaddr_in as suggested in the man pages,
		 * this iface name will come along for the ride.
		 * (see div_output for the other half of this.)
		 */
		ksnprintf(divsrc.sin_zero, sizeof divsrc.sin_zero,
			 m->m_pkthdr.rcvif->if_xname);
	}

	/* Put packet on socket queue, if any */
	sa = NULL;
	nport = htons((u_int16_t)port);
	LIST_FOREACH(inp, &divcbinfo.pcblisthead, inp_list) {
		if (inp->inp_flags & INP_PLACEMARKER)
			continue;
		if (inp->inp_lport == nport)
			sa = inp->inp_socket;
	}
	if (sa) {
		if (ssb_appendaddr(&sa->so_rcv, (struct sockaddr *)&divsrc, m,
				 (struct mbuf *)NULL) == 0)
			m_freem(m);
		else
			sorwakeup(sa);
	} else {
		m_freem(m);
		ipstat.ips_noproto++;
		ipstat.ips_delivered--;
	}
}

#ifdef SMP
static void
div_packet_handler(struct netmsg *nmsg)
{
	struct netmsg_packet *nmp;
	struct lwkt_msg *msg;
	struct mbuf *m;
	int port, incoming = 0;

	nmp = (struct netmsg_packet *)nmsg;
	m = nmp->nm_packet;

	msg = &nmsg->nm_lmsg;
	port = msg->u.ms_result32 & 0xffff;
	if (msg->u.ms_result32 & DIV_INPUT)
		incoming = 1;

	div_packet(m, incoming, port);
}
#endif	/* SMP */

void
divert_packet(struct mbuf *m, int incoming, int port)
{
	/* Sanity check */
	KASSERT(port != 0, ("%s: port=0", __func__));
	M_ASSERTPKTHDR(m);

	/* Assure header */
	if (m->m_len < sizeof(struct ip) &&
	    (m = m_pullup(m, sizeof(struct ip))) == NULL)
		return;

	/* Sanity check */
	KASSERT(m_tag_find(m, PACKET_TAG_IPFW_DIVERT, NULL) != NULL,
		("%s no divert tag!", __func__));

#ifdef SMP
	if (mycpuid != 0) {
		struct netmsg_packet *nmp;
		struct lwkt_msg *msg;

		nmp = &m->m_hdr.mh_netmsg;
		netmsg_init(&nmp->nm_netmsg, &netisr_apanic_rport, 0,
			    div_packet_handler);
		nmp->nm_packet = m;

		msg = &nmp->nm_netmsg.nm_lmsg;
		msg->u.ms_result32 = port; /* port is 16bits */
		if (incoming)
			msg->u.ms_result32 |= DIV_INPUT;
		else
			msg->u.ms_result32 |= DIV_OUTPUT;

		lwkt_sendmsg(cpu_portfn(0), &nmp->nm_netmsg.nm_lmsg);
	} else
#endif
	div_packet(m, incoming, port);
}

/*
 * Deliver packet back into the IP processing machinery.
 *
 * If no address specified, or address is 0.0.0.0, send to ip_output();
 * otherwise, send to ip_input() and mark as having been received on
 * the interface with that address.
 */
static int
div_output(struct socket *so, struct mbuf *m,
	struct sockaddr_in *sin, struct mbuf *control)
{
	int error = 0;
	struct m_tag *mtag;

	if (control)
		m_freem(control);		/* XXX */

	/*
	 * Prepare the tag for divert info. Note that a packet
	 * with a 0 tag in mh_data is effectively untagged,
	 * so we could optimize that case.
	 */
	mtag = m_tag_get(PACKET_TAG_IPFW_DIVERT, sizeof(u_int16_t), MB_DONTWAIT);
	if (mtag == NULL) {
		error = ENOBUFS;
		goto cantsend;
	}
	m_tag_prepend(m, mtag);

	/* Loopback avoidance and state recovery */
	if (sin)
		*(u_int16_t *)m_tag_data(mtag) = sin->sin_port;
	else
		*(u_int16_t *)m_tag_data(mtag) = 0;

	/* Reinject packet into the system as incoming or outgoing */
	if (DIV_IS_OUTPUT(sin)) {
		struct ip *const ip = mtod(m, struct ip *);

		/* Don't allow packet length sizes that will crash */
		if ((u_short)ntohs(ip->ip_len) > m->m_pkthdr.len) {
			error = EINVAL;
			goto cantsend;
		}

		/* Convert fields to host order for ip_output() */
		ip->ip_len = ntohs(ip->ip_len);
		ip->ip_off = ntohs(ip->ip_off);

		/* Send packet to output processing */
		ipstat.ips_rawout++;			/* XXX */
		error = ip_output(m, NULL, NULL,
			    (so->so_options & SO_DONTROUTE) |
			    IP_ALLOWBROADCAST | IP_RAWOUTPUT,
			    NULL, NULL);
	} else {
		ip_input(m);
	}
	return error;

cantsend:
	m_freem(m);
	return error;
}

static int
div_attach(struct socket *so, int proto, struct pru_attach_info *ai)
{
	struct inpcb *inp;
	int error;

	inp  = so->so_pcb;
	if (inp)
		panic("div_attach");
	if ((error = suser_cred(ai->p_ucred, NULL_CRED_OKAY)) != 0)
		return error;

	error = soreserve(so, div_sendspace, div_recvspace, ai->sb_rlimit);
	if (error)
		return error;
	error = in_pcballoc(so, &divcbinfo);
	if (error)
		return error;
	inp = (struct inpcb *)so->so_pcb;
	inp->inp_ip_p = proto;
	inp->inp_vflag |= INP_IPV4;
	inp->inp_flags |= INP_HDRINCL;
	/*
	 * The socket is always "connected" because
	 * we always know "where" to send the packet.
	 */
	so->so_state |= SS_ISCONNECTED;
	return 0;
}

static int
div_detach(struct socket *so)
{
	struct inpcb *inp;

	inp = so->so_pcb;
	if (inp == NULL)
		panic("div_detach");
	in_pcbdetach(inp);
	return 0;
}

static int
div_abort(struct socket *so)
{
	soisdisconnected(so);
	return div_detach(so);
}

static int
div_disconnect(struct socket *so)
{
	if (!(so->so_state & SS_ISCONNECTED))
		return ENOTCONN;
	return div_abort(so);
}

static int
div_bind(struct socket *so, struct sockaddr *nam, struct thread *td)
{
	int error;

	/*
	 * in_pcbbind assumes that nam is a sockaddr_in
	 * and in_pcbbind requires a valid address. Since divert
	 * sockets don't we need to make sure the address is
	 * filled in properly.
	 * XXX -- divert should not be abusing in_pcbind
	 * and should probably have its own family.
	 */
	if (nam->sa_family != AF_INET) {
		error = EAFNOSUPPORT;
	} else {
		((struct sockaddr_in *)nam)->sin_addr.s_addr = INADDR_ANY;
		error = in_pcbbind(so->so_pcb, nam, td);
	}
	return error;
}

static int
div_shutdown(struct socket *so)
{
	socantsendmore(so);
	return 0;
}

static int
div_send(struct socket *so, int flags, struct mbuf *m, struct sockaddr *nam,
	 struct mbuf *control, struct thread *td)
{
	/* Length check already done in ip_mport() */
	KASSERT(m->m_len >= sizeof(struct ip), ("IP header not in one mbuf"));

	/* Send packet */
	return div_output(so, m, (struct sockaddr_in *)nam, control);
}

SYSCTL_DECL(_net_inet_divert);
SYSCTL_PROC(_net_inet_divert, OID_AUTO, pcblist, CTLFLAG_RD, &divcbinfo, 0,
	    in_pcblist_global, "S,xinpcb", "List of active divert sockets");

struct pr_usrreqs div_usrreqs = {
	.pru_abort = div_abort,
	.pru_accept = pru_accept_notsupp,
	.pru_attach = div_attach,
	.pru_bind = div_bind,
	.pru_connect = pru_connect_notsupp,
	.pru_connect2 = pru_connect2_notsupp,
	.pru_control = in_control,
	.pru_detach = div_detach,
	.pru_disconnect = div_disconnect,
	.pru_listen = pru_listen_notsupp,
	.pru_peeraddr = in_setpeeraddr,
	.pru_rcvd = pru_rcvd_notsupp,
	.pru_rcvoob = pru_rcvoob_notsupp,
	.pru_send = div_send,
	.pru_sense = pru_sense_null,
	.pru_shutdown = div_shutdown,
	.pru_sockaddr = in_setsockaddr,
	.pru_sosend = sosend,
	.pru_soreceive = soreceive,
	.pru_sopoll = sopoll
};
