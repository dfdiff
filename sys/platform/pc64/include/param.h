/*
 * $DragonFly: src/sys/platform/pc64/include/param.h,v 1.1 2007/09/23 04:42:07 yanyh Exp $
 */

#ifndef _MACHINE_PARAM_H_

#ifndef _NO_NAMESPACE_POLLUTION
#define _MACHINE_PARAM_H_
#endif

#ifndef _MACHINE_PLATFORM
#define _MACHINE_PLATFORM	pc64
#endif

#ifndef _NO_NAMESPACE_POLLUTION

#ifndef MACHINE_PLATFORM
#define MACHINE_PLATFORM	"pc64"
#endif

#endif

#include <cpu/param.h>

#endif

